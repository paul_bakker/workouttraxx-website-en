#!/usr/bin/env bash

VERSION=`git describe`
curl -o output.txt  -XPOST http://10.151.16.64:8000/deployment -d '{
  "deploymentType": "blue-green",
  "namespace": "workouttraxx",
  "useHealthCheck": false,
  "newVersion": "'$VERSION'",
  "appName": "website-en",
  "replicas": 1,
  "frontend": "cloudrti.workouttraxx.com",
  "podspec": {
    "containers": [
    {
        "image": "nginx",
        "name" : "nginx",
        "ports": [{
          "containerPort": 80
        }],
        "volumeMounts": [{
            "mountPath": "/usr/share/nginx/html",
            "name": "www",
            "readOnly": true
        }]
    },

    {
      "image": "paulbakker/git-sync",
      "name": "git-sync",
      "imagePullPolicy" : "Always",
      "env": [
        {
            "name": "GIT_SYNC_REPO",
            "value": "https://paul_bakker@bitbucket.org/paul_bakker/workouttraxx-website-en.git"
        },
        {
            "name": "GIT_SYNC_WAIT",
            "value": "10"
        },
        {
          "name" : "GIT_SYNC_BRANCH",
          "value" : "master"
        }
      ],
      "volumeMounts": [{
        "mountPath": "/git",
        "name": "www"
      }]
    }],

    "volumes": [{
      "name": "www",
      "emptyDir": {}
    }]
  }
}'

cat output.txt

FAILED=$(cat output.txt | grep "Deployment Failed" | wc -l)
if [ $FAILED -ne 0 ]; then
    exit 1
fi

exit 0